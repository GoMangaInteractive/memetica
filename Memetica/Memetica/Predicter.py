'''
    Memetica AI - A neural network that can recognize memes

    Copyright (C) 2018 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from __future__ import division, print_function, absolute_import

from skimage import color, io
from scipy.misc import imresize
import numpy as np
from sklearn.model_selection import train_test_split
import os
from glob import glob

import tflearn
from tflearn.data_utils import shuffle, to_categorical
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.estimator import regression
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation
from tflearn.metrics import Accuracy

class Predicter(object):
    """
        Predicter Class:
            This Class is used to evaluate an image with an existent neural network
    """

    def __init__(self, aiFile = 'memetica_model.tflearn'):
        """
            Predicter class constructor:
                Initializes the AI and loads the neural network
        """
        #Normalize the images
        img_prep = ImagePreprocessing()
        img_prep.add_featurewise_zero_center()
        img_prep.add_featurewise_stdnorm()

        #Create extra synthetic training data by flipping & rotating images
        img_aug = ImageAugmentation()
        img_aug.add_random_flip_leftright()
        img_aug.add_random_rotation(max_angle=25.)

        #Input is a 32x32 image with 3 color channels (red, green and blue)
        network = input_data(shape=[None, 64, 64, 3], data_preprocessing=img_prep, data_augmentation=img_aug)

        #1: Convolution layer with 32 filters, each 3x3x3
        conv_1 = conv_2d(network, 32, 3, activation='relu', name='conv_1')

        #2: Max pooling layer
        network = max_pool_2d(conv_1, 2)

        #3: Convolution layer with 64 filters
        conv_2 = conv_2d(network, 64, 3, activation='relu', name='conv_2')

        #4: Convolution layer with 64 filters
        conv_3 = conv_2d(conv_2, 64, 3, activation='relu', name='conv_3')

        #5: Max pooling layer
        network = max_pool_2d(conv_3, 2)

        #6: Fully-connected 512 node layer
        network = fully_connected(network, 512, activation='relu')

        #7: Dropout layer to combat overfitting
        network = dropout(network, 0.5)

        #8: Fully-connected layer with two outputs
        network = fully_connected(network, 2, activation='softmax')

        #Configure how the network will be trained
        acc = Accuracy(name="Accuracy")
        network = regression(network, optimizer='adam', loss='categorical_crossentropy', learning_rate=0.0005, metric=acc)

        #Wrap the network in a model object
        self.AI = tflearn.DNN(network, checkpoint_path='model_memes.tflearn', max_checkpoints = 3, tensorboard_verbose = 0, tensorboard_dir='tmp/tflearn_logs/')
        self.AI.load(aiFile)


    def Evaluate(self, imagePath, fileSize = 64):
        """
            Evaluate function:
                This function loads an input image and evaluates it using the loaded AI
        """
        img = np.array(imresize(io.imread(imagePath), (fileSize, fileSize, 3)), dtype = 'float32')
        img = np.reshape(img, (1, 64, 64, 3))
        print("Memetica is evaluating your image...")
        isAmeme = self.AI.predict(img)

        return isAmeme
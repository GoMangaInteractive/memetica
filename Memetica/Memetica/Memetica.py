'''
    Memetica AI - A neural network that can recognize memes

    Copyright (C) 2018 Francesco Pio Squillante

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''
import warnings
warnings.filterwarnings("ignore")

import Trainer
import Predicter

import os

if os.environ.get('OS','') == 'Windows_NT':
    clear = "cls"
else:
    clear = "clear"

os.system(clear)

print("Memetica AI - A neural network that can recognize memes")
print("Written by Francesco Pio Squillante for educational purpouse\n")

risp = input("Do you want to re-train the AI? (y/n) ")

#Neural Network re-training
if risp == "y":
    print("\nLoading all the images, please wait...")
    tr = Trainer.Trainer()
    epochs = int(input("For how many epochs you want to train the AI? (recommended = 100) "))
    print("\nMemetica is learning about your memes. This will take a while...")
    tr.StartTraining(epochs = epochs, Verbose = True)
    print("\n\nEverything done. Please restart memetica...")
    input()
    exit(0)

print("\nLet's test Memetica's intelligence!\n")

#Startup the AI
print("Starting up the AI...\n")
ai = Predicter.Predicter()

os.system(clear)

while True:
    filePath = input("\nWhere is your meme stored? ")

    results = ai.Evaluate(filePath)

    if results[0][1] > 0.5:
        print("\nMemetica thinks your image is a meme! Funny!")
        print("Probability that is a meme: {0:.2f}%".format(results[0][1]*100))
    else:
        print("\nMemetica thinks your image is not a meme. Not funny :c")
        print("Probability that is a meme: {0:.2f}%".format(results[0][1]*100))

    risp = input("Do you want to test with another image? (y/n) ")
    if risp == "n":
        break